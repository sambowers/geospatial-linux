.. _refresher:

Refresher for Linux
===================

These instructions are a stripped-down version of those from :ref:`command_line`, designed to refesh the memory of those who have previously used the Linux command line. If you've recently run through :ref:`command_line`, you can skip this page. If this page moves too quickly, return to :ref:`command_line` to run through this in more detail.

The Linux terminal
------------------

Recall that Linux has both a graphical interface, similar to those you'll be sued with Windows or Mac operating systems, but also a more powerful text interface. We call this text interface the 'terminal'

How to open the terminal depends on your Linux distribution. You'll usually find the terminal at ``Applications -> System Tools -> Terminal``. Or, right click the Desktop and there is usually a shortcut to open a terminal window.

The terminal window will look something like this:

.. image:: images/blank_terminal.png
   :scale: 50 %
   :align: center

The terminal shows your username (in my case, ``sbowers3``), the name of the computer you're connected to (``mein``), the directory you're in (``~``) and has a space for you to type commands (``□``).

Recall that you can type commands into the terminal, and execute them with the ``return`` key. For instance, type ``echo Hello World!`` into ther terminal, and execute it::

    [username@linuxpc ~] echo Hello World!
    Hello World!

The ``echo`` command is very simple, it prints text to the terminal. There exist a large number of command line tools within Linux, take a look at some of `these`_ and try out a few that you think might be useful.

.. _these: https://en.wikipedia.org/wiki/List_of_Unix_commands

Navigation
----------

Where am I?
~~~~~~~~~~~

.. image:: images/penguin1.gif
   :align: center

The Linux file system is based on a series of directories. In Windows, these are called 'folders'.

You can get the find out where you are with the ``pwd`` (print working directory) command::
    
    [username@linuxpc ~] pwd
    /home/username

On most systems when you open a new terminal window you'll be located in your home directory. The home directory is located at ``/home/username``, which can also be reached with the shortcut ``~``. The home space is the part of the system that you have control over, and is the default location for file storage.

The root of the directory system is ``/``, with each subsequent ``/`` separating directory names.

What's here?
~~~~~~~~~~~~

.. image:: images/penguin2.gif
   :align: center

The command ``ls`` (list) will tell you what files and directories are in the working directory::

    [username@linuxpc ~] ls
    Desktop
    Documents
    Downloads
    ...

To show more details about the contents of the working directory, you can use the ``-lh`` (long, human readable) flags::

    [username@linuxpc ~] ls -l
    drwxr-xr-x  4 username username 4.0K Sep  7 11:32 Desktop
    drwxr-xr-x 12 username username 4.0K Sep 12 11:35 Documents
    drwxr-xr-x  9 username username  12K Sep 13 13:14 Downloads
    ...

This additionally gives you infomrmation the about file permissions (e.g. ``drwxr-xr-x``), file ownder (``username``), the file size (e.g. ``4.0K``), modification date and time (e.g. ``Sep 12 11:35``).

It's worth taking a little time to understand file permissions. This string is split into four sections:

.. image:: images/file_permissions.png
   :align: center

The first character gives the file type, and the remaining nine characters the read, write and execute permissions for the owner, group members and others users.

Moving around
~~~~~~~~~~~~~

.. image:: images/penguin3.gif
   :align: center

You can move into other directories of the system with the ``cd`` (change directory) command::

    [username@linuxpc ~] cd Documents
    [username@linuxpc Documents] pwd
    /home/username/Documents

You can move back up the directory tree with  ``..``::

    [username@linuxpc Documents] cd ..
    [username@linuxpc ~] pwd
    /home/username

Similarly, you can move two steps up the directory tree with ``../..``::

    [username@linuxpc ~] cd ../..
    [username@linuxpc /] pwd
    /

With the above commands we've been using 'relative' file paths. They refer to a location that relative to the present working directory. We can also use absolute pathnames which do not take account of the working directory. Absolute paths always start from the root ``//`` directory, for example::

    [username@linuxpc ~] cd /home/username/Documents
    
You can move back to the home directory with the shortcut ``'~'``::
    
    [username@linuxpc /] cd ~
    [username@linuxpc ~] pwd
    /home/username

Making and moving files
-----------------------

Manipulating files
~~~~~~~~~~~~~~~~~~

First, we'll make a test file to play with using the command ``touch``::

    [username@linuxpc ~] cd ~
    [username@linuxpc ~] touch file1
    [username@linuxpc ~] ls
    ...
    file1
    ...

We can also make a new directory using the command ``mkdir`` (make directory)::

    [username@linuxpc ~] mkdir directory1
    [username@linuxpc ~] ls
    ...
    directory1
    file1
    ...

We can move our file into the directory using the ``mv`` command::

    [username@linuxpc ~] mv file1 directory1

Change directory to ``directory1`` and verify that ``file1`` is there.

We can use the ``cp`` command to copy a file::
    
    [username@linuxpc directory1] cp file1 file2
    [username@linuxpc directory1] ls
    file1
    file2

Files can be deleted with the ``rm`` (remove) command::

    [username@linuxpc directory1] rm file2 
    
We can run similar commands to ``cp`` and ``rm`` entire directories. Note in these cases we need to use the ``-r`` flag, which stands for recursive. This means perform the command on the directory and all its contents::

    [username@linuxpc directory1] cd ..
    [username@linuxpc ~] cp -r directory1 directory2
    [username@linuxpc ~] rm -r directory2

Creating a text file
~~~~~~~~~~~~~~~~~~~~

We can launch programs from the command line. A common example of this will be to create text files. There exist a lot of text editors for this purpoes (e.g. ``vim``, ``emacs``, ``kate``), here we'll use ``gedit``. Use gedit to create a new file called newfile.txt and save it to the home directory. Close gedit when you're done.::
    
    [username@linuxpc ~] gedit
    
.. image:: images/gedit.png
   :scale: 50 %
   :align: center

To edit ``newfile.txt`` in gedit, you can call gedit with the filename:::

    [username@linuxpc ~] gedit newfile.txt

Changing file permissions
~~~~~~~~~~~~~~~~~~~~~~~~~

Recall that you can use ``ls -l`` to view file permissions. Imagine we wanted to give other users permission to read our text file. For this we can use the command ``chmod`` (change mode).

To use ``chmod`` we need to specify three options relating to (i) what person or group, (ii) whether the permission should be added or removed, and (iii) which permission should be changed. These are encoded as:

=============  ====================  ==============
  Who?           What?                Which?
=============  ====================  ==============
u = owner      \+ add permission     r = read
g = group      \- remove permission  w = write
o = others                           x = execute
a = all
=============  ====================  ==============

For example, to give other users permisson to read our file::

    [username@linuxpc ~] chmod o+r newfile.txt

In this case ``o`` refers to others, ``+`` to add the specified mode, and ``r`` to read-access.

Running programs
----------------

The command line can be used to execute commands or scripts. You've already run a few commands from the command line (``ls``, ``pwd``, ``gedit``, ``chmod``, ``cat``), so you already know how to eecute programs

Simple programs
~~~~~~~~~~~~~~~

Some simple programs can be executed with a single word. What do the following programs do?::

    [username@linuxpc ~] whoami
    [username@linuxpc ~] fortune
    [username@linuxpc ~] xeyes
    
Programs with options
~~~~~~~~~~~~~~~~~~~~~

Lots of programs have options, which we can modify with ``--flags``.

Let's take the ``xeyes`` program as an example. By default, ``xeyes`` operates as follows::
    
    [username@linuxpc ~] xeyes
    
.. image:: images/xeyes1.png
   :align: center

We can change the colour of the eyes using ``-fg`` (foreground colour) and choosing a colour::

    [username@linuxpc ~] xeyes -fg green

.. image:: images/xeyes2.png
   :align: center

Flags are a very common way of specifying input options. There is a command called ``man`` (manual). We can use it to get help for most command line programs. For the example of ``xeyes``::

    [username@linuxpc ~] man xeyes
    XEYES(1)                    General Commands Manual                   XEYES(1)

    NAME
        xeyes - a follow the mouse X demo

    SYNOPSIS
        xeyes [-option ...]

    DESCRIPTION
        Xeyes watches what you do and reports to the Boss.

    OPTIONS
        -fg foreground color
                choose a different color for the pupil of the eyes.

        -bg background color
                choose a different color for the background.

        -outline outline color
                choose a different color for the outline of the eyes.

        -center center color
                choose a different color for the center of the eyes.
    ...

Try some of these  options to remind yourself how flags work.

Shortcuts and tricks
--------------------

Wildcards
~~~~~~~~~

Often we will want to refer to multiple filenames or directories. We do this with the wildcard symbols ``*`` and ``?``.  The ``?`` a single character, and the ``*`` symbol refers or zero or more characters.

For example, we might want to list everything beginning with ``D`` in the home directory::

    [username@linuxpc ~] ls -l D*
    drwxr-xr-x  4 username username 4.0K Sep  7 11:32 Desktop
    drwxr-xr-x 12 username username 4.0K Sep 12 11:35 Documents
    drwxr-xr-x  9 username username  12K Sep 13 13:14 Downloads

We'll return to using wildcard symbols later.
    
Auto complete
~~~~~~~~~~~~~

We can use the ``tab`` key to auto complete commmands in the terminal. For example, if we wanted to ``cd`` to the ``Documents`` directory we could start with::

    [username@linuxpc ~] cd Doc

... hit tab, and as there are no other directories fitting that pattern, the field will autofill to Documents::

    [username@linuxpc ~] cd Documents/

Where there is more than one possible program, directory, or files, the tab key will display possible options::
    
    [username@linuxpc ~] cd Do
    Documents/ Downloads/ 

Fill in more letters and press ``tab`` again to proceed.

You'll find that use of the tab key to auto complete will speed up navigation around the command line substantially.

Get previous commands
~~~~~~~~~~~~~~~~~~~~~

You can use the ``up`` key to scroll through a command history. This will save you having to re-type out commands repeatedley. Once you find the command you're looking for, you can edit it with the ``left`` and ``right`` keys. Give it a try!

Summary
-------

We learned how to use the following commands. There are many thousands of other commands in Linux which you'll learn over time, but these are the main commands you'll need to navigate and manipulate files on the command line.

* ``echo``: Print to the terminal
* ``pwd``: Print working directory.
* ``ls``: List contents of working directory
* ``cd``: Change directory
* ``mkdir``: Make directory
* ``mv``: Move
* ``cp``: Copy
* ``rm``: Remove
* ``chmod``: Change file permissions
* ``gedit``: Create a text file in the gedit editor
* ``man``: View manual
* ``ssh``: Connect to another PC
