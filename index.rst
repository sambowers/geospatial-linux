Introduction to Geospatial Linux
================================

Welcome to this very short introduction to using Linux for geospatial applications.

We will start with the very basics of the Linux, and then look at how Linux command line tools can be used to process, analyse and interpret geospatial data.

Our aim is to introduce you to some of the fundamental concepts of the Linux command line. This is **not** designed to be a thorough tutorial, but will introduce you to the most important concepts to be able to get started using Linux.

We will aim to build up this documentation over time to give you a record of the methods we've covered. Take note of the URL, these notes may be something that you'll refer back to in future.

Aims
----

By the end of this tutorial, we hope that you will be able to:

* Navigate the Linux command line
* Execute programs from the Linux command line
* Use some key GDAL command line tools to manipulate geospatial data
* Understand how to ssh to other PCs or servers

Instructions
------------

* Work through each input, and make it run on your own PC
* Do not copy-paste. You'll learn a lot by entering each command manually
* When you encounter an error, in the first instance try and work out the issue yourself. If you're still stuck, do ask for some pointers
* Don't worry about making mistakes, you can't break Linux
* Using Google to help with exercises is encouraged
* Go at your own pace; we don't expect you to reach the end of this tutorial today.

.. NOTE::
    For those of you with some previous Linux command line experience, we recommmend skipping forwards to the :ref:`refresher` page.

Contents
--------

.. toctree::
   :maxdepth: 2
   :numbered:
   
   command_line.rst
   gdal.rst
   refresher.rst
   to_be_continued.rst
   
Search
------

* :ref:`search`

.. Indices and tables
.. ------------------

.. * :ref:`genindex`
.. * :ref:`modindex

.. image:: images/UoE_Horizontal_Logo_CMYK_v1_160215.jpg
   :scale: 20 %

.. image:: images/ltsi_logo.jpg
