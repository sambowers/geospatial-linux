To be continued...
==================

Further reading
---------------

Getting used to Linux is tough. The best way to learn is by doing.

If you would like to learn more about Linux and GDAl, I can suggest the following resources:

* `Ryans Tutorials`_: A very nicely-written command line tutorial.
* `University of Surrey`_: A detailed guide to Linux
* This `GDAL cheat sheet`_: Learn GDAL by example
* `Google`_: honestly.

.. _Ryans Tutorials: https://ryanstutorials.net/linuxtutorial/commandline.php
.. _University of Surrey: http://www.ee.surrey.ac.uk/Teaching/Unix/
.. _GDAL cheat sheet: https://github.com/dwtkns/gdal-cheat-sheet
.. _Google: https://www.google.com

Stay in touch
-------------

Please do email either Sam (sam.bowers@ed.ac.uk) or Simone (simone-vaccari@ltsi.co.uk) if you'd like any assistance or any further tips. We'll be very happy to help.

.. figure:: images/linux_xkcd.png
    
    Souce: `XKCD`_.

.. _XKCD: https://xkcd.com/456/

